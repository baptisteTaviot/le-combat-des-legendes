#include <SFML/Graphics.hpp>
#include <time.h>
//#include "mouvement.hpp"


#define long_fen 1600
#define haut_fen 900
using namespace sf;

typedef struct
{
    int x;
    int y;
    int vie;
    int angle;

} joueur;
/**la caract�ristique angle d�signe la direction dans laquelle le perso regarde: 1= droite, 0=gauche*/


int main()
{
    joueur j1,j2;
    //initialisation donn�es des joueurs
    j1.x=800;
    j1.y=600;
    j2.x=1200;
    j2.y=600;
    j1.vie=10000;
    j2.vie=10000;
    //
    int tempssaut1=0;
    int tempsdroite1=0;
    int tempsgauche1=0;

    int tempssaut2=0;
    int tempsdroite2=0;
    int tempsgauche2=0;

    int countAj1=0;
    int countBj1=0;
    int countAj2=0;
    int countBj2=0;




    //cr�ation de la fenetre
    RenderWindow app(VideoMode(long_fen, haut_fen), "Le combat des l�gendes");
    app.setFramerateLimit(60);

    /**cr�ation de la forme des joueurs*/
    RectangleShape joueur1(Vector2f(90,200));//joueur 1
    joueur1.setPosition(j1.x,j1.y);
    joueur1.setFillColor(Color::Blue);

    RectangleShape joueur2(Vector2f(90,200));//joueur 2
    joueur2.setPosition(j2.x,j2.y);
    joueur2.setFillColor(Color::Red);
    /**cr�ation des hitboxes des attaques des joueurs*/
    RectangleShape coupAj1(Vector2f(40,40));//atk A joueur 1
    coupAj1.setFillColor(Color::White);

    RectangleShape coupBj1(Vector2f(800,40));//atk B joueur 1
    coupBj1.setFillColor(Color::White);

    RectangleShape coupAj2(Vector2f(40,40));//atk A joueur 2
    coupAj2.setFillColor(Color::Green);

    RectangleShape coupBj2(Vector2f(800,40));//atk B joueur 2
    coupBj2.setFillColor(Color::Green);


    /**UTILISATION DES TOUCHES*/
    while (app.isOpen())
    {

        Event event;

        /**MOUVEMENT-
        -joueur1*/
        if(Keyboard::isKeyPressed(Keyboard::Right))//aller � droite
        {
            if(j1.x+10<long_fen-200)
                tempsdroite1=5;
            j1.angle=1;
        }
        if(Keyboard::isKeyPressed(Keyboard::Left))//aller � gauche
        {
            if(j1.x-10>110)

                tempsgauche1=5;
            j1.angle=0;
        }
        if(Keyboard::isKeyPressed(Keyboard::Up))//sauter
        {
            if(j1.y==600)
                tempssaut1=20;
        }
        /**joueur 2*/
        if(Keyboard::isKeyPressed(Keyboard::D))//aller � droite
        {
            if(j2.x+10<long_fen-200)
                tempsdroite2=5;
            j2.angle=1;
        }
        if(Keyboard::isKeyPressed(Keyboard::Q))//aller � gauche
        {
            if(j2.x-10>110)

                tempsgauche2=5;
            j2.angle=0;


        }
        if(Keyboard::isKeyPressed(Keyboard::Z))//sauter
        {
            if(j2.y==600)
                tempssaut2=20;
        }
        /**ATTAQUES
        joueur1*/
        if(Keyboard::isKeyPressed(Keyboard::Numpad1))//attaque A
        {
            countAj1=5;
            if(j1.angle==1)
            {
                coupAj1.setPosition(j1.x+90,j1.y);
            }
            else
            {
                coupAj1.setPosition(j1.x-40,j1.y);
            }
        }

        if(Keyboard::isKeyPressed(Keyboard::Numpad2))//attaque B
        {
            countBj1=5;
            if(j1.angle==1)
                coupBj1.setPosition(j1.x+90,j1.y);
            else
                coupBj1.setPosition(j1.x-800,j1.y);

        }
        /**joueur 2*/
        if(Keyboard::isKeyPressed(Keyboard::Num1))//attaque A
        {
            countAj2=5;
            if(j2.angle==1)
            {

                coupAj2.setPosition(j2.x+90,j2.y);
            }
            else
            {
                coupAj2.setPosition(j2.x-40,j2.y);
            }
        }
        if(Keyboard::isKeyPressed(Keyboard::Num2))//attaque B
        {
            countBj2=5;
            if(j2.angle==1)
            {
                coupBj2.setPosition(j2.x+90,j2.y);
            }
            else
            {
                coupBj2.setPosition(j2.x-800,j2.y);
            }
        }
        //////////////////////
        if(event.key.code==Keyboard::V)//pour afficher la vie
                {
                    printf("\nvie du joueur 1 : %i\nvie du joueur 2 : %i\n",j1.vie,j2.vie);
                }
        /////////////////////
        /** pour fermer la fenetre*/
        while (app.pollEvent(event))
        {
            if (event.type == Event::Closed)
                app.close();
        }

        /**MOUVEMENT PREMIER JOUEUR*/
        if(tempssaut1!=0)
        {
            if(tempssaut1>=11)
            {
                j1.y-=20;

            }
            else if (tempssaut1<11)
            {
                j1.y+=20;
            }
            tempssaut1-=1;
        }
        if(tempsdroite1!=0)
        {
            j1.x+=15;
            tempsdroite1-=1;
        }
        if(tempsgauche1!=0)
        {
            j1.x-=15;
            tempsgauche1-=1;
        }


        /**MOUVEMENT DEUXIEME JOUEUR*/
        if(tempssaut2!=0)
        {
            if(tempssaut2>=11)
            {
                j2.y-=20;

            }
            else if(tempssaut2<11)
            {
                j2.y+=20;
            }
            tempssaut2-=1;
        }
        if(tempsdroite2!=0)
        {
            j2.x+=15;
            tempsdroite2-=1;
        }
        if(tempsgauche2!=0)
        {
            j2.x-=15;
            tempsgauche2-=1;
        }



        /**DETECTION DES DEGATS*/

        /**si j2 touch� par attaque A du j1*/
        if(countAj1>0)
        {
            if(j1.angle==1)
            {
                if((j1.x+90<=j2.x&&j1.x+90+40>=j2.x)||(j1.x+90<=j2.x+90&&j1.x+90+40>=j2.x+90)||(j1.x+90>=j2.x&&j1.x+90+40<=j2.x+90))
                {
                    //test de hauteur
                    printf("il y a une collision\n");
                    j2.vie-=100;

                }
                else
                    printf("pas de collision\n");
            }
            else
            {
                if((j1.x-40<=j2.x&&j1.x>=j2.x)||(j1.x-40<=j2.x+90&&j1.x>=j2.x+90)||(j1.x-40>=j2.x&&j1.x<=j2.x+90))
                {
                    //test de hauteur
                    printf("il y a une collision\n");
                    j2.vie-=100;
                }
                else
                    printf("pas de collision\n");
            }
        }



        /**si j2 touch� par attaque B du j1*/



        /**si j1 touch� par attaque A du j2*/
        if(countAj2>0)
        {
            if(j2.angle==1)
            {
                if((j2.x+90<=j1.x&&j2.x+90+40>=j1.x)||(j2.x+90<=j1.x+90&&j2.x+90+40>=j1.x+90)||(j2.x+90>=j1.x&&j2.x+90+40<=j1.x+90))
                {
                    //test de hauteur
                    printf("il y a une collision\n");
                    j1.vie-=100;

                }
                else
                    printf("pas de collision\n");
            }
            else
            {
                if((j2.x-40<=j1.x&&j2.x>=j1.x)||(j2.x-40<=j1.x+90&&j2.x>=j1.x+90)||(j2.x-40>=j1.x&&j2.x<=j1.x+90))
                {
                    //test de hauteur
                    printf("il y a une collision\n");
                    j1.vie-=100;
                }
                else
                    printf("pas de collision\n");
            }



        }
        /**si j1 touch� par attaque B du j2*/
        if(countBj2>0)
        {
            if(j2.angle==1)//j2 regarde � droite
            {
                if((j2.x+90<=j1.x&&j2.x+90+800>=j1.x)||(j2.x+90<=j1.x&&j2.x+90+800>=j1.x+90)||(j2.x+90<=j1.x+90&&j2.x+90+800>=j1.x+90))
                {
                    //test de hauteur
                    printf("il y a une collision\n");
                    j1.vie-=50;

                }
                else
                    printf("pas de collision\n");
            }
            else
            {
                if((j2.x>=j1.x&&j2.x-800<=j1.x)||(j2.x>=j1.x+90&&j2.x-800<=j1.x)||(j2.x>=j1.x+90&&j2.x-800>=j1.x))
                {
                    //test de hauteur
                    printf("il y a une collision\n");
                    j1.vie-=50;
                }
                else
                    printf("pas de collision\n");
            }



        }
        ////////////////////////


        app.clear();
        joueur1.setPosition(j1.x,j1.y);
        joueur2.setPosition(j2.x,j2.y);

        app.draw(joueur1);
        app.draw(joueur2);

        if(countAj1>0)
        {
            app.draw(coupAj1);
            countAj1-=1;
        }

        if(countBj1>0)
        {
            app.draw(coupBj1);
            countBj1-=1;
        }

        if(countAj2>0)
        {
            app.draw(coupAj2);
            countAj2-=1;
        }

        if(countBj2>0)
        {
            app.draw(coupBj2);
            countBj2-=1;
        }





        app.display();



    }

    return EXIT_SUCCESS;
}
